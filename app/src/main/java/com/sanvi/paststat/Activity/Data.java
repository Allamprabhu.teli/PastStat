package com.sanvi.paststat.Activity;

/**
 * Created by sanvi on 10/8/17.
 */

public class Data {
    private String description;
    private String studentMarks;
    private String percentage;
    private String progress;

    public Data(String studentMarks) {
        this.studentMarks = studentMarks;
    }

    public Data(String description , String studentMarks, String percentage,String progress) {

        this.description = description;
        this.studentMarks = studentMarks;
        this.percentage  = percentage;
        this.progress = progress;
    }


    public String getDescription() {
        return description;
    }

    public String getStudentMarks(){
        return studentMarks;
    }

    public String getPercentage(){
        return percentage;
    }
    public String getProgress(){
        return progress;
    }


}
