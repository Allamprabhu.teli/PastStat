package com.sanvi.paststat.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.sanvi.paststat.Interfaces.RetrofitArrayApi;
import com.sanvi.paststat.Manager.LoginManager;
import com.sanvi.paststat.Manager.UserManager;
import com.sanvi.paststat.PojoClasses.Student;
import com.sanvi.paststat.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


public class CurrentideaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView mNavigationUserName;
    private ImageView mNavigationUserImage;
    private UserManager mUserManager;
    private LoginManager mLoginManger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currentidea);
        mUserManager = new UserManager(this);
        mLoginManger = new LoginManager(this);

        Log.i("hhhhhhhhhhhh","hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh"+mUserManager.getUserName());
        Log.i("ppppppppp","ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp"+mUserManager.getUserImage());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Live Ideas");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);
        mNavigationUserName  = (TextView)headerLayout.findViewById(R.id.nav_user_name);

        mNavigationUserImage  =(ImageView)headerLayout.findViewById(R.id.nav_user_image);

        mNavigationUserName.setText(mUserManager.getUserName());

        Picasso.with(CurrentideaActivity.this)
                .load(mUserManager.getUserImage())
                .placeholder(R.mipmap.ic_launcher_round) //this is optional the image to display while the url image is downloading
                .error(R.mipmap.ic_launcher_round)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                .into(mNavigationUserImage);
        Log.i("ppppppppp","ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp"+mUserManager.getUserImage());


        new Swipe(this).swipeCard();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_item_refresh) {

            new Swipe(this).swipeCard();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert)
                .setMessage(R.string.wanttoexit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.currentidea, menu);
        return true;



    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.live_idea) {
           Intent i  = new Intent(this, CurrentideaActivity.class);
            startActivity(i);
            finish();

        } else if (id == R.id.closed_idea) {
            Intent i = new Intent(this, ClosedIdea.class);
            startActivity(i);

        } else if (id == R.id.rate_us) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=PackageName")));

        } else if (id == R.id.invite_friends) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Try the paststat App. Powered by: paststat download link is http://www.google.com");
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.invite_using)));

        } else if (id == R.id.feedback) {
//            final Intent _Intent = new Intent(android.content.Intent.ACTION_SEND);
//            _Intent.setType("text/html");
//            _Intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ getString(R.string.mail_feedback_email) });
//            _Intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.mail_feedback_subject));
//            _Intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.mail_feedback_message));
//            startActivity(Intent.createChooser(_Intent, getString(R.string.title_send_feedback)));


            Intent i = new Intent(this,FeedBackActivity.class);
            startActivity(i);
        }

        else if (id == R.id.user_guide) {
            Intent i = new Intent(this, UserGuideActivity.class);
            startActivity(i);

        }else if (id == R.id.privacy) {
            Intent i = new Intent(this,PrivacyActivity.class);
            startActivity(i);

        }else if (id == R.id.signout) {

            new AlertDialog.Builder(this)
                    .setMessage(R.string.wanttosignout)
                    .setTitle(R.string.signout)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                                mUserManager.setUserName(null);
                                mUserManager.setUserMail(null);
                                mUserManager.setUserfbid(null);
                                mUserManager.setUserImage(null);
                                mLoginManger.setFirstTimeLaunch(true);
                            finish();
                        }


                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}

