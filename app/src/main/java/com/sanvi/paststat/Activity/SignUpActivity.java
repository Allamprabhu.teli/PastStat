package com.sanvi.paststat.Activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sanvi.paststat.Manager.LoginManager;
import com.sanvi.paststat.Manager.UserManager;
import com.sanvi.paststat.R;
import com.facebook.appevents.AppEventsLogger;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.util.Arrays;

import io.fabric.sdk.android.Fabric;
import retrofit.Response;
import retrofit.Retrofit;


public class SignUpActivity extends AppCompatActivity {

    public CallbackManager mFacebookCallbackManager;
    public Button mFacebookSignInButton;
    public SignInButton mGoogleSignInButton;
    public TwitterLoginButton mTwitterSignInButton;
    public Profile profile;
    private String name;
    private UserManager mUserManager;
    private LoginManager mLoginManager;
    private ProgressDialog mDialog;


    Button or_email;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker mProfileTracker;
    GoogleApiClient mGoogleApiClient;

    public void currentIdea(View view){
        Intent i = new Intent(this,CurrentideaActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);


        mUserManager = new UserManager(this);
        mLoginManager = new LoginManager(this);

        TwitterAuthConfig authConfig = new TwitterAuthConfig("9Nh7lAUTOXHdBhXyoVEnmPXI9",
                "Xepyzzhd7h6jucIvYlERB8mVAnIPjb6y1S3SKYIJBvCD9RAlv0");
        Fabric.with(this, new TwitterCore(authConfig));



        FacebookSdk.sdkInitialize(getApplicationContext());
        mFacebookCallbackManager = CallbackManager.Factory.create();
        mFacebookSignInButton = (LoginButton) findViewById(R.id.facebook_sign_in_button);
        setContentView(R.layout.activity_sign_up);

        //ggggooooooooooooooooooooooggggggggggglllllllllllleeeeeeeessssstart...

        mGoogleSignInButton = (SignInButton) findViewById(R.id.google_sign_in_button);
        mGoogleSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGoogle();
            }
        });

//        mTwitterSignInButton = (TwitterLoginButton)findViewById(R.id.twitter_sign_in_button);
        mTwitterSignInButton = (TwitterLoginButton)findViewById(R.id.twitter_sign_in_button);
        mTwitterSignInButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void onResponse(Response<TwitterSession> response, Retrofit retrofit) {

            }

            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void success(final Result<TwitterSession> result) {
                // handleSignInResult(...);
            }
            @Override
            public void failure(TwitterException e) {
                // handleSignInResult(...);
            }
        });

        mTwitterSignInButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void onResponse(Response<TwitterSession> response, Retrofit retrofit) {

            }

            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void success(final Result<TwitterSession> result) {

                // handleSignInResult(...);
            }

            @Override
            public void failure(TwitterException e) {
                // handleSignInResult(...);
            }
        });


        //jjjjjjjjjjjjjjjjj
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };
        accessTokenTracker.startTracking();
        LoginButton mFacebookSignInButton = (LoginButton) findViewById(R.id.facebook_sign_in_button);
        mFacebookSignInButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        mFacebookSignInButton.registerCallback(mFacebookCallbackManager, callback);

        or_email = (Button) findViewById(R.id.facebook_sign_in_button);

        or_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(SignUpActivity.this,SignUpActivity.class);
//                SignUpActivity.this.startActivity(i);
            }


        });


    }

    private static final int RC_SIGN_IN = 9001;

    private void signInWithGoogle() {

        Log.i("sssssssssssss","Login googllllleeeeeeeee");

        getDetails();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        final Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }


    public FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {

        @Override
        public void onSuccess(LoginResult loginResult) {
            try {
                if (Profile.getCurrentProfile() == null) {
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile profile_old, Profile profile_new) {
                            profile = profile_new;
                            Log.v("facebook - profile", profile_new.getFirstName());

                            Toast.makeText(SignUpActivity.this, profile.getFirstName(), Toast.LENGTH_LONG).show();
                            Log.i("iiiiiiii", "ppppppppppppppppppp" + profile.getProfilePictureUri(200, 200));

                            String name = profile.getFirstName();
                            String lname = profile.getLastName();
                            String fbid = profile.getId();
                            String uPic = String.valueOf(profile.getProfilePictureUri(400, 400));
                            String fullname = name + " " + lname;
                            Intent i = new Intent(getApplicationContext(), CurrentideaActivity.class);
                            Log.i("fffffbbb", "fffbbbffffffffbbbfbf" + fbid);
                            mUserManager.setUserName(fullname);
                            mUserManager.setUserImage(uPic);
                            mUserManager.setUserfbid(fbid);
                            mUserManager.setUserMail(profile.getId());
                            mLoginManager.setFirstTimeLaunch(false);

                            startActivity(i);
                            mProfileTracker.stopTracking();
                        }
                    };
                    mProfileTracker.startTracking();
                } else {
                    profile = Profile.getCurrentProfile();
                    Log.v("facebook - profile", profile.getFirstName());
                }


                name = profile.getFirstName();

                Intent intent = new Intent(getApplicationContext(), CurrentideaActivity.class);
                intent.putExtra("name", name);
                startActivity(intent);
                finish();


//    iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii

            } catch (Exception e) {
                Log.d("ERROR", e.toString());
            }
        }

        @Override
        public void onCancel() {
            Toast.makeText(getApplicationContext(), R.string.cancelled, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(FacebookException e) {
            Log.i("FACEBOOK ERRROR", e.toString());
            Toast.makeText(getApplicationContext(), R.string.somethingwentwrongplztryagain, Toast.LENGTH_LONG).show();
        }
    };

    private void getDetails(){
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.pleasewait));
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(false);
        mDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        mFacebookCallbackManager.onActivityResult(requestCode, responseCode, intent);







        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);

            if (result.isSuccess()) {


                final GoogleApiClient client = mGoogleApiClient;


                Toast.makeText(this, result.getSignInAccount().getDisplayName(), Toast.LENGTH_LONG).show();

                mUserManager.setUserName(result.getSignInAccount().getDisplayName());
                mUserManager.setUserMail(result.getSignInAccount().getEmail());
                mUserManager.setUserImage(String.valueOf(result.getSignInAccount().getPhotoUrl()));
                mLoginManager.setFirstTimeLaunch(false);


                Log.i("iiiiiiiii", "jjjjjjjjjjjjjjjjjjjjjjjjjjjjj" + result.getSignInAccount().getDisplayName() + "     " + result.getSignInAccount().getPhotoUrl());
                Intent i = new Intent(SignUpActivity.this, CurrentideaActivity.class);
                startActivity(i);
                mDialog.dismiss();
                finish();



                //handleSignInResult(...)
            } else {
                mDialog.dismiss();
                //handleSignInResult(...);
            }
        } else {
            // Handle other values for requestCode

        }
        if(TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE == requestCode) {
            mTwitterSignInButton.onActivityResult(requestCode, responseCode, intent);
        }


    }
}







