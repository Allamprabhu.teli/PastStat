package com.sanvi.paststat.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.sanvi.paststat.Manager.LoginManager;
import com.sanvi.paststat.R;
import io.fabric.sdk.android.Fabric;

public class Splash extends AppCompatActivity {
    private LoginManager mLoginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        mLoginManager = new LoginManager(this);
//        loginCall();
        new FetchData().execute();

    }


    private class FetchData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            if (!mLoginManager.isFirstTimeLaunch()) {
            mLoginManager.setFirstTimeLaunch(false);
            Intent intent = new Intent(Splash.this, CurrentideaActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(Splash.this, SignUpActivity.class);
            startActivity(intent);
            finish();

        }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            finish();
        }

    }


//    public void loginCall() {
//        if (!mLoginManager.isFirstTimeLaunch()) {
//            mLoginManager.setFirstTimeLaunch(false);
//            Intent intent = new Intent(this, CurrentideaActivity.class);
//            startActivity(intent);
//            finish();
//        } else {
//            Intent intent = new Intent(this, SignUpActivity.class);
//            startActivity(intent);
//            finish();
//
//        }
//
//    }
}
