package com.sanvi.paststat.Activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sanvi.paststat.Interfaces.FeedBackApi;
import com.sanvi.paststat.Manager.LoginManager;
import com.sanvi.paststat.Manager.UserManager;
import com.sanvi.paststat.PojoClasses.FeedBackPojo;
import com.sanvi.paststat.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FeedBackActivity extends AppCompatActivity {

    private UserManager mUserManager;
    private LoginManager mLoginManager;
    private ProgressDialog mDialog;
    private EditText  mDataet;
    private String mName;
    private String mAccountId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        mDataet= (EditText)findViewById(R.id.feedbackText);

        mUserManager = new UserManager(this);
        mLoginManager = new LoginManager(this);
        mName= mUserManager.getUserName();
        mAccountId = mUserManager.getUserMail();
    }

     public void SendFeedback(View view) {
         mDialog = new ProgressDialog(this);
         mDialog.setMessage(getResources().getString(R.string.Sending_FeedBack));
         mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
         mDialog.setCancelable(false);
         mDialog.show();

         try {
             Retrofit.Builder builder = new Retrofit.Builder()
//                 Base url to be set afterwards.............................................
                     .baseUrl("")
                     .addConverterFactory(GsonConverterFactory.create());

             Retrofit retrofit = builder.build();
             FeedBackApi service = retrofit.create(FeedBackApi.class);
             Call<FeedBackPojo> call = service.CreateFeedback(mDataet.getText().toString(), mName, mAccountId);

             call.enqueue(new Callback<FeedBackPojo>() {
                 @Override
                 public void onResponse(Call<FeedBackPojo> call, Response<FeedBackPojo> response) {
                     mDialog.dismiss();
                     Toast.makeText(getApplicationContext(), R.string.messagesent, Toast.LENGTH_LONG).show();
                 }

                 @Override
                 public void onFailure(Call<FeedBackPojo> call, Throwable t) {
                     Toast.makeText(getApplicationContext(), R.string.pleasetryagain, Toast.LENGTH_LONG).show();

                 }
             });
         }catch (Exception e) {
             mDialog.dismiss();
             Toast.makeText(getApplicationContext(), R.string.urlnotfound,Toast.LENGTH_LONG).show();
             e.printStackTrace();
         }
     }
}
