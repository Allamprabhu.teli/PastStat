package com.sanvi.paststat.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.sanvi.paststat.Interfaces.RetrofitArrayApi;
import com.sanvi.paststat.PojoClasses.Student;
import com.sanvi.paststat.R;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by sanvi on 14/9/17.
 */

public class Swipe {

    public static MyAppAdapter myAppAdapter;
    public static ViewHolder viewHolder;
    private ArrayList<Data> array;
    private SwipeFlingAdapterView flingContainer;
    private ProgressDialog mDialog;
    public String mPercentage;
    private Activity activity;

    public Swipe(Activity activity) {
        this.activity = activity;
    }

    private void getDetails() {
        mDialog = new ProgressDialog(activity);
        mDialog.setMessage("Please Wait");
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setCancelable(true);
        mDialog.show();
    }


    public  void swipeCard(){
        getDetails();
        flingContainer = (SwipeFlingAdapterView)activity.findViewById(R.id.frame1);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://androidtutorialpoint.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitArrayApi service = retrofit.create(RetrofitArrayApi.class);

        Call<List<Student>> call = service.getStudentDetails();

        call.enqueue(new Callback<List<Student>>() {
            @Override
            public void onResponse(Response<List<Student>> response, Retrofit retrofit) {

//                Log.i("jjjjjjjjjjjjjjjj","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj"+array.get(0).getDescription());

                try {
                    List<Student> StudentData = response.body();
                    array = new ArrayList<Data>();
                    for (int i = 0; i < StudentData.size(); i++) {
                        Log.i("hhhhhhhhhhhhhhhh", "iiiiiiiiiiiiiiiiiiiiiiiiiiiiiii" + StudentData.get(i).getStudentName());
                        Data a = new Data(StudentData.get(i).getStudentName(),StudentData.get(i).getStudentMarks(),StudentData.get(i).getStudentMarks(),StudentData.get(i).getStudentMarks());
                        array.add(a);
                        mPercentage= StudentData.get(i).getStudentMarks();
                        mDialog.dismiss();
                    }

                }
                catch(Exception e){
                    mDialog.dismiss();
                    e.printStackTrace();
                }
                myAppAdapter = new MyAppAdapter(array,activity);
                flingContainer.setAdapter(myAppAdapter);
                myAppAdapter.notifyDataSetChanged();
                flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
                    @Override
                    public void removeFirstObjectInAdapter() {

                    }

                    @Override
                    public void onLeftCardExit(Object dataObject) {
                        array.remove(0);
                        myAppAdapter.notifyDataSetChanged();
                        //Do something on the left!
                        //You also have access to the original object.
                        //If you want to use it just cast it (String) dataObject
                    }

                    @Override
                    public void onRightCardExit(Object dataObject) {

                        array.remove(0);
                        myAppAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onAdapterAboutToEmpty(int itemsInAdapter) {
                    }

                    @Override
                    public void onScroll(float scrollProgressPercent) {

                        View view = flingContainer.getSelectedView();
                        view.findViewById(R.id.background).setAlpha(0);
                    }
                });
                // Optionally add an OnItemClickListener
                flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClicked(int itemPosition, Object dataObject) {

                        View view = flingContainer.getSelectedView();
                        view.findViewById(R.id.background).setAlpha(0);

                        myAppAdapter.notifyDataSetChanged();
                    }
                });

            }
            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
        //swipe code starts here.............................................

    }


    public static class ViewHolder {
        public static FrameLayout background;
        public TextView mDataText,mDataMarks,mPercentage;
        public ProgressBar mProgressCircle;

    }

    public class MyAppAdapter extends BaseAdapter {


        public List<Data> parkingList;
        public Context context;

        private MyAppAdapter(List<Data> apps, Context context) {
            this.parkingList = apps;
            this.context = context;
        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;

            //iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii

            if (rowView == null) {

//                LayoutInflater inflater = getLayoutInflater();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                rowView = inflater.inflate(R.layout.item_current_idea, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.mDataText = (TextView) rowView.findViewById(R.id.bookText);
                viewHolder.mDataMarks = (TextView) rowView.findViewById(R.id.marks);
                viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
                viewHolder.mPercentage= (TextView) rowView.findViewById(R.id.tv);
                viewHolder.mProgressCircle=(ProgressBar)rowView.findViewById(R.id.circularProgressbar);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.mDataText.setText(parkingList.get(position).getDescription()+" ");
            viewHolder.mDataMarks.setText(parkingList.get(position).getStudentMarks());
            viewHolder.mPercentage.setText(parkingList.get(position).getStudentMarks()+"%");
            viewHolder.mProgressCircle.setProgress(Integer.parseInt(parkingList.get(position).getStudentMarks()));
            return rowView;
        }
    }
}
