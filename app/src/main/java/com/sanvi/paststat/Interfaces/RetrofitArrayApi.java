package com.sanvi.paststat.Interfaces;

import com.sanvi.paststat.PojoClasses.Student;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by sanvi on 29/8/17.
 */

public interface RetrofitArrayApi {
    @GET("api/RetrofitAndroidArrayResponse")
    Call<List<Student>> getStudentDetails();
}


