package com.sanvi.paststat.Interfaces;

import com.sanvi.paststat.PojoClasses.FeedBackPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by sanvi on 5/9/17.
 */

public interface FeedBackApi {
//    @FormUrlEncoded
//    @POST(" ")
//    public Call<FeedBackPojo> CreateFeedback();

    @POST("")
    @FormUrlEncoded
    Call<FeedBackPojo> CreateFeedback(@Field("title") String name,
                        @Field("body") String accountId,
                        @Field("userId") String feedbackData);
}
