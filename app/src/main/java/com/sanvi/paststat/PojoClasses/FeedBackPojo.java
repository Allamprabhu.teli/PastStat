package com.sanvi.paststat.PojoClasses;

/**
 * Created by sanvi on 5/9/17.
 */

public class FeedBackPojo {
    private String name;
    private String accountId;
    private String feedbackData;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFeedbackData() {
        return feedbackData;
    }

    public void setFeedbackData(String feedbackData) {
        this.feedbackData = feedbackData;
    }
}
