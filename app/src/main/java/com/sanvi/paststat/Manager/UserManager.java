package com.sanvi.paststat.Manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sanvi on 23/8/17.
 */

public class UserManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "USERNAME_DATA";

    private static final String SAVE_USERNAME = "SAVE_USERNAME";
    private static final String SAVE_USERIMAGE ="SAVE_USERNIMAGE";

    private static final String SAVE_USERMAIL = "SAVE_USERMAIL";
    private static final String SAVE_USERFBID = "SAVE_USERFBID";
    private static final boolean IS_FIRST_TIME = true;

    public UserManager(Context context) {

        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setUserName(String name) {
        editor.putString(SAVE_USERNAME, name);
        editor.commit();
    }

    public String getUserName() {

        return pref.getString(SAVE_USERNAME, null);
    }

    public void setUserImage(String image) {
        editor.putString(SAVE_USERIMAGE, image);
        editor.commit();
    }

    public String getUserImage() {

        return pref.getString(SAVE_USERIMAGE, null);
    }
    public void setUserMail(String mail) {
        editor.putString(SAVE_USERMAIL, mail);
        editor.commit();
    }

    public String getUserMail() {

        return pref.getString(SAVE_USERMAIL, null);
    }

    public void setUserfbid(String fbid) {
        editor.putString(SAVE_USERFBID, fbid);
        editor.commit();
    }

    public boolean getIsFirstTime(){
        return pref.getBoolean(String.valueOf(IS_FIRST_TIME),true);
    }

    public void setIsFirstTime(Boolean isFirstTime)
    {
        editor.putBoolean(String.valueOf(IS_FIRST_TIME), isFirstTime);
        editor.commit();
    }
}
