package com.sanvi.paststat.Adapater;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sanvi.paststat.Activity.Data;
import com.sanvi.paststat.R;
import java.util.ArrayList;

/**
 * Created by sanvi on 9/8/17.
 */


public class ClosedAdapter extends RecyclerView.Adapter<ClosedAdapter.RecyclerViewHolder> {
    private LayoutInflater inflater;
    private ArrayList<Data> Student;
    private Context context;

    public ClosedAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.cardrow, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.mclosedcompanyName1.setText(Student.get(position).getStudentMarks());
        holder.mclosed_detail_date1.setText(Student.get(position).getPercentage());
        holder.mclosed_detail_date2.setText(Student.get(position).getDescription());
        holder.mclosedprofit1.setText(Student.get(position).getStudentMarks());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        public TextView mclosedcompanyName1, mclosedprofit1, mclosed_detail_date1;
        public TextView mclosedcompanyname2, mclosedprofit2, mclosed_detail_date2;


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            mclosedcompanyName1 = (TextView) itemView.findViewById(R.id.closedcompanyName1);
            mclosedcompanyname2 = (TextView) itemView.findViewById(R.id.closedcompanyname2);
            mclosed_detail_date1 = (TextView) itemView.findViewById(R.id.closed_detail_date1);
            mclosed_detail_date2 = (TextView) itemView.findViewById(R.id.closed_detail_date2);
            mclosedprofit1 = (TextView) itemView.findViewById(R.id.closedprofit1);
            mclosedprofit2 = (TextView) itemView.findViewById(R.id.closedprofit2);

        }


    }
}